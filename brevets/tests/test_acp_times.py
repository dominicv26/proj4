""" Nose tests for acp_times algorithm"""

import nose
import arrow
from acp_times import open_time, close_time


def test_open_single_control_times():
	now = arrow.get('2020-01-01 00:00', 'YYYY-MM-DD HH:mm')
	assert open_time(0, 1000, now) == arrow.get('2020-01-01 00:00', 'YYYY-MM-DD HH:mm')
	assert open_time(100, 1000, now) == arrow.get('2020-01-01 02:56', 'YYYY-MM-DD HH:mm')
	assert open_time(200, 1000, now) == arrow.get('2020-01-01 05:53', 'YYYY-MM-DD HH:mm')
	assert open_time(400, 1000, now) == arrow.get('2020-01-01 12:08', 'YYYY-MM-DD HH:mm')
	assert open_time(600, 1000, now) == arrow.get('2020-01-01 18:48', 'YYYY-MM-DD HH:mm')
	assert open_time(1000, 1000, now) == arrow.get('2020-01-02 09:05', 'YYYY-MM-DD HH:mm')

def test_close_single_control_times():
	now = arrow.get('2020-01-01 00:00', 'YYYY-MM-DD HH:mm')
	assert close_time(0, 1000, now) == arrow.get('2020-01-01 01:00', 'YYYY-MM-DD HH:mm')
	assert close_time(100, 1000, now) == arrow.get('2020-01-01 06:40', 'YYYY-MM-DD HH:mm')
	assert close_time(200, 1000, now) == arrow.get('2020-01-01 13:20', 'YYYY-MM-DD HH:mm')
	assert close_time(400, 1000, now) == arrow.get('2020-01-02 02:40', 'YYYY-MM-DD HH:mm')
	assert close_time(600, 1000, now) == arrow.get('2020-01-02 16:00', 'YYYY-MM-DD HH:mm')
	assert close_time(1000, 1000, now) == arrow.get('2020-01-04 03:00', 'YYYY-MM-DD HH:mm')

def test_empty_brevet():
	now = arrow.utcnow().format('YYYY-MM-DD HH:mm')
	assert open_time(0,0,now) == now
	assert close_time(0,0,now) == now


def test_random_controls_and_time():
	now = arrow.utcnow()
	assert open_time(0, 300, now) == now.shift(hours=+0, minutes=+0)
	assert close_time(0, 300, now) == now.shift(hours=+1, minutes=+0)

	assert open_time(69, 300, now) == now.shift(hours=+2, minutes=+2)
	assert close_time(69, 300, now) == now.shift(hours=+4, minutes=+36)

	assert open_time(172, 300, now) == now.shift(hours=+5, minutes=+4)
	assert close_time(172, 300, now) == now.shift(hours=+11, minutes=+28)

	assert open_time(212, 300, now) == now.shift(hours=+6, minutes=+15)
	assert close_time(212, 300, now) == now.shift(hours=+14, minutes=+8)

	assert open_time(258, 300, now) == now.shift(hours=+7, minutes=+42)
	assert close_time(258, 300, now) == now.shift(hours=+17, minutes=+12)

	assert open_time(300, 300, now) == now.shift(hours=+9, minutes=+1)
	assert close_time(300, 300, now) == now.shift(hours=+20, minutes=+0)

def test_control_over_total():
	now = arrow.get('2020-01-01 00:00', 'YYYY-MM-DD HH:mm')
	assert open_time(350, 300, now) == open_time(300, 300, now)
	assert close_time(350, 300, now) == close_time(300, 300, now)

def test_control_under_50():
	now = arrow.get('2020-01-01 00:00', 'YYYY-MM-DD HH:mm')
	assert open_time(25, 200, now) == arrow.get('2020-01-01 00:44', 'YYYY-MM-DD HH:mm')
	assert close_time(25, 200, now) == arrow.get('2020-01-01 01:40', 'YYYY-MM-DD HH:mm')




