"""
Open and close time calculations
for ACP-sanctioned brevets
following rules described at https://rusa.org/octime_alg.html
and https://rusa.org/pages/rulesForRiders
"""
import arrow
import math


"""
###############################
Assumptions
###############################
1) minutes are rounded by standard conventions (>=0.5 up, <0.5 down) unlike the website
    which always rounds down?
2) Control locations are exclusive on the opening distance, e.g. 
   [0,200]
   (200, 400] <==> [201,400]
3) last control time should match total brevet distance. If over, will simply 
    return the time as if the control is the total brevet distance.
4) admins should not place controls too close to the start, no special measures
   are taken (be wary any control less than 50km)
5) if not a number is entered, does nothing
"""


def time_arithmetic(time: float):
  hours = int(math.floor(time))
  minutes = int(round((time - hours) * 60))
  return hours, minutes


def calc_time(bounds_dict: dict, control_km: int, start_time: 'arrow object'):
    end_time = start_time
    control = control_km

    for lower_bound in bounds_dict:
        if control > lower_bound:
            amount_in_range = control - lower_bound
            hrs, mins = time_arithmetic(amount_in_range / bounds_dict[lower_bound])
            end_time = end_time.shift(hours=+hrs, minutes=+mins)
            control -= amount_in_range
    return end_time  


def open_time(control_dist_km, brevet_dist_km, brevet_start_time):
    """
    Args:
       control_dist_km:  number, the control distance in kilometers
       brevet_dist_km: number, the nominal distance of the brevet
           in kilometers, which must be one of 200, 300, 400, 600,
           or 1000 (the only official ACP brevet distances)
       brevet_start_time:  An ISO 8601 format date-time string indicating
           the official start time of the brevet
    Returns:
       An ISO 8601 format date string indicating the control open time.
       This will be in the same time zone as the brevet start time.
    """
    bounds_for_max_speed = {
        1000: 26, # (1000,1300]
        600: 28,  # (600, 1000]
        400: 30,  # (400, 600]
        200: 32,  # (200, 400]
        0: 34     # (0, 200]
    }
    if brevet_dist_km == 0:
        return brevet_start_time

    if control_dist_km > brevet_dist_km:
        end_time = calc_time(bounds_for_max_speed, brevet_dist_km, brevet_start_time)
    else:
        end_time = calc_time(bounds_for_max_speed, control_dist_km, brevet_start_time)
    return end_time


def close_time(control_dist_km, brevet_dist_km, brevet_start_time):
    """
    Args:
       control_dist_km:  number, the control distance in kilometers
          brevet_dist_km: number, the nominal distance of the brevet
          in kilometers, which must be one of 200, 300, 400, 600, or 1000
          (the only official ACP brevet distances)
       brevet_start_time:  An ISO 8601 format date-time string indicating
           the official start time of the brevet
    Returns:
       An ISO 8601 format date string indicating the control close time.
       This will be in the same time zone as the brevet start time.
    """
    bounds_for_min_speed = {
        1000: 40/3, # (1000,1300]
        600: 11.428,  # (600, 1000]
        400: 15,  # (400, 600]
        200: 15,  # (200, 400]
        0: 15     # (0, 200]
    }
    if control_dist_km == 0:
        if brevet_dist_km == 0:
            return brevet_start_time
        else:
            end_time = brevet_start_time.shift(hours=+1)

    else:
        if control_dist_km > brevet_dist_km:
            end_time = calc_time(bounds_for_min_speed, brevet_dist_km, brevet_start_time)
        else:
            end_time = calc_time(bounds_for_min_speed, control_dist_km, brevet_start_time)
    return end_time





