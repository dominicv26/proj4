# Brevet time calculator with Ajax

Implementation the RUSA ACP controle time calculator with flask and ajax. Credits to Michal Young for the initial version of this code.

## ACP controle times

That's "controle" with an 'e', because it's French, although "control" is also accepted. Controls are points where a rider must obtain proof of passage, and control[e] times are the minimum and maximum times by which the rider must arrive at the location. Background information is given here (https://rusa.org/pages/rulesForRiders). The algorithm for calculating controle times is explained below.

### Algorithm

This algorithm implements the minimum and maximum control speeds for ACP brevets given by the chart here (https://rusa.org/pages/acp-brevet-control-times-calculator). Calculcations are done by dividing the control location by the min/max speed to give close/open times in a Date hour:minute military time format (see example below). In this implementation, control locations are exclusive on the range start and inclusive on the range end (e.g. the 400-600 range maps to (400,600]). For a comprehensive example, consider a 600km brevet with controls at 50km, 200km, 250km, and 600km that starts on January 1st at 00:00. The calculations for open and close times are as follows:
Open times:
50km (max speed = 34 km/hr) -> 01:28
200km (max speed = 34 km/hr) -> 05:53
250km (max speed = 32 km/hr) -> 07:27
300km (max speed = 30 km/hr) -> 9:01

Close times:
50km (min speed = 15 km/hr) -> 03:20
200km (min speed = 15 km/hr) -> 13:20
250km (min speed = 15 km/hr) -> 16:40
300km (min speed = 15 km/hr) -> 20:00

###  Notes

* Note that the times are being calculated by a simple formula. We divide the control location by by either the min or max speed given during that range of which the control lands in. The floor of this number (e.g. 3.14 -> 3) gives the number of hours. Then the fractional part is multiplied by 60 to get the number of minutes (e.g. 3.14 -> (3.14 - 3) * 60 = 8.4). In the case that the number of minutes is not an integer, standard rounding conventions are used; i.e. anything with a fractional part <0.5 gets rounded down, and a fractional part >=0.5 gets rounded up. 

* The last control time should match the total brevet distance. Otherwise, the calculation will use the total brevet distance instead of the control distance. For example, if you place a control at 305km on a 300km brevet, the program will NOT calculate the times for the 305km control and will instead compute it for a 300km control.

* Administrators should be wary of placing controls less than 50km of the start. No special measures are taken for time oddities that happen in the beginning few kilometers of a race

* A default closing time of +1 hour is given when the first control location is 0km, to give the athletes time to get started. 


## AJAX and Flask reimplementation

This implementation of the brevet time calculator is done with AJAX and Flask. Only thing to note here is that no calculation will be performed in the case that an input field is populated with anything besides a number. 

## Usage

Clone the repo with the following command:
``` git clone https://dominicv26@bitbucket.org/dominicv26/proj4.git ```
Either run ``` docker build . ``` to use a docker for running the web application. Then run 
``` docker run -d -p 5000:5000 <imageID>```
To run the web application on port 5000. 
Otherwise, if all dependencies are installed on a local machine, run 
```python flask_brevets``` 
Where the default port is given as 5000.

## Testing

A suite of nose test cases is ran automatically upon building of the Docker image. Otherwise, run the following command from the brevets/ directory to run the test suite:
``` nosetests ```

